<?php

namespace Drupal\testimoodul\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class RegistrationForm.
 */
class RegistrationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'registration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['first_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First name'),
      '#required' => TRUE,
    ];

    $form['last_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last name'),
      '#required' => TRUE,
    ];

    $form['phone_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone number'),
      '#required' => TRUE,
    ];

    $form['gender'] = [
      '#type' => 'radios',
      '#title' => $this->t('Gender'),
      '#options' => [
        'male' => $this->t('Male'),
        'female' => $this->t('Female')
      ],
      '#required' => TRUE,
    ];

    $form['age'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Age'),
      '#required' => TRUE,
    ];

    $form['country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#options' => $this->getCountryList(),
      '#required' => TRUE,
    ];

    $form['number_of_tickets'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of tickets'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Set form rebuild to true, so we can save inserted data
    $form_state->setRebuild(TRUE);

    // Get submitted values
    $first_name = $form_state->getValue('first_name');
    $last_name = $form_state->getValue('last_name');
    $gender = $form_state->getValue('gender');
    $age = $form_state->getValue('age');
    $country = $form_state->getValue('country');
    $visited_countries = $form_state->getValue('visited_countries');
    $number_of_tickets = $form_state->getValue('number_of_tickets');
    $phone_number = $form_state->getValue('phone_number');
    $full_name = $this->getFullName($first_name, $last_name);

    $information = [
      'gender' => $gender,
      'age' => $age,
      'country' => $country,
      'visited countries' => $visited_countries
    ];

    $phone_valid = $this->validatePhoneNumber($phone_number, $country);

    $tickts_validates = $this->validateTicketCount($number_of_tickets);


    if($age>18) {
      if(!$phone_valid){
        $this->messenger()->addError('Please check you phone number');
      }elseif(!$tickts_validates){
        $this->messenger()->addError('You can add up to 99 tickets!');
      }
      else {
        $tickets = $this->generateTickets($number_of_tickets);
        $this->messenger()->addMessage("Thank you, $full_name!");
        $this->messenger()->addMessage('here are you tickets:');
        foreach ($tickets as $key => $ticket) {
          $ticket_no = $key + 1;
          $this->messenger()->addMessage("$ticket_no: $ticket");
        }
        $this->messenger()->addMessage('Other information about you:');
        foreach ($information as $key => $data) {
          if ($key !== 'visited countries') {
            $this->messenger()->addMessage("$key: $data");
          }
        }
      }
    }else{
      $this->messenger()->addError('Unfortunately you are not old enough. Sorry!');
    }
  }

  public function getFullName($first_name, $last_name){
    return "$first_name $last_name";
  }

  public function getCountryList(){
    return [
      'EE' => 'Estonia',
      'LV' => 'Latvia',
      'LT' => 'Lithuania'
    ];
  }
  // ESIMENE ARENDUSTSÜKKEL - Telefoni numbri valideerimine
  public function validatePhoneNumber($phone_number, $country) {
    //@todo: add strlen kontroll
    // Kui telo on ainult numbrid
    if (!is_numeric($phone_number)) {
      return FALSE;
    }
    // Estonia
    if ($country === 'EE') {
      //check phone number length
      if (!(strlen($phone_number) === 11 || strlen($phone_number) === 12)) {
        return FALSE;
      }
      //check country code
      if (strpos($phone_number, '+372') !== 0) {
        return FALSE;
      }
      //check rest of the number
      if (!is_numeric(substr($phone_number, 4))) {
        return FALSE;
      }
    }
    // Lithuania
    if ($country === 'LT') {
      //check phone number length
      if (strlen($phone_number) !== 12) {
        return FALSE;
      }
      //check country code
      if (strpos($phone_number, '+370') !== 0) {
        return FALSE;
      }
      //check rest of the number
      if (!is_numeric(substr($phone_number, 4))) {
        return FALSE;
      }
    }
    // Latvia
    if ($country === 'LV') {
      //check phone number length
      if (strlen($phone_number)  !== 12) {
        return FALSE;
      }
      //check country code
      if (strpos($phone_number, '+371') !== 0) {
        return FALSE;
      }
      //check rest of the number
      if (!is_numeric(substr($phone_number, 4))) {
        return FALSE;
      }
    }
    return TRUE;
  }

  // TEINE ARENDUSTSÜKKEL - Piletite genereerimine
  public function generateTickets($number_of_tickets){

    $tickets = [];

    for($i = 0; $i < $number_of_tickets; $i++){
      $result = '';
      for($j = 0; $j < 7; $j++) {
        $result .= mt_rand(0, 9);
      }
      $tickets[] = $result;
    }
    return $tickets;
  }

  public function validateTicketCount($number_of_tickets){
    if($number_of_tickets >= 100){
      return FALSE;
    }
    return TRUE;
  }

  // KOLMAS ARENDUSTSÜKKEL - Vanuse kontroll
  public function allowedToRegister($age){
    if((int)$age >= 18){
      return TRUE;
    }
    return FALSE;
  }



}
