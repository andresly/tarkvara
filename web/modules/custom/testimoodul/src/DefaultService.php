<?php

namespace Drupal\testimoodul;

/**
 * Class DefaultService.
 */
class DefaultService implements DefaultServiceInterface {

  /**
   * Constructs a new DefaultService object.
   */
  public function __construct() {

  }

  public function addingNumbers($a, $b){
    $result = $a + $b;
    return $result;
  }

}
